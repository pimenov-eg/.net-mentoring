﻿using System;
using System.Linq.Expressions;
using ExpressionTree.Task1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class ExpressionVisitorTests
    {
        [TestMethod]
        public void IncExpressionTest()
        {
            Expression<Func<int, int>> originalExpression = (a) => a + 1;

            var transformedExpression = new IncDecExpressionVisitor().Visit(originalExpression) as Expression<Func<int, int>>;
            var result = transformedExpression.Compile().Invoke(1);
            
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void DecExpressionTest()
        {
            Expression<Func<int, int>> originalExpression = (a) => a - 1;

            var transformedExpression = new IncDecExpressionVisitor().Visit(originalExpression) as Expression<Func<int, int>>;
            var result = transformedExpression.Compile().Invoke(2);
            
            Assert.AreEqual(1, result);
        }
    }
}
