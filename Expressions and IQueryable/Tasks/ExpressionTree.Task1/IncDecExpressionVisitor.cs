﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTree.Task1
{
    public class IncDecExpressionVisitor : ExpressionVisitor
    {
        public override Expression Visit(Expression node)
        {
            if (node == null)
                return base.Visit(node);

            var binaryExpression = node as BinaryExpression;
            if (binaryExpression != null &&
                binaryExpression.Left.NodeType == ExpressionType.Parameter && binaryExpression.Right.NodeType == ExpressionType.Constant &&
                (int)((ConstantExpression)binaryExpression.Right).Value == 1)
            {
                if (binaryExpression.NodeType == ExpressionType.Add)
                    return  base.Visit(Expression.Increment(binaryExpression.Left));
                if (binaryExpression.NodeType == ExpressionType.Subtract)
                    return base.Visit(Expression.Decrement(binaryExpression.Left));
            }

            return base.Visit(node);
        }
    }
}